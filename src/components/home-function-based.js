// FUNCTIONAL BASED COMPONENT

import React from "react";

import { NavLink /* , useHistory */ } from "react-router-dom";

import { useOktaAuth } from "@okta/okta-react";

const Home = () => {
  
  const { authState, authService } = useOktaAuth();
    
  const login = (event) => {
    event.preventDefault();
    // return 
    authService.login("/profile");
  };

  const logout = (event) => {
    event.preventDefault();
    // return
    authService.logout("/"); // uri parameter is optional

    // CULL THE COOKIES
    // care of... https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

    // CULL ANY LOCALSTORAGE
    window.localStorage.clear();
    
  };

  const elPending = (
    <>
      <div className="card-body">
        {/* Konichiwa!!! */}

        Your request is pending and loading&hellip;
      </div>
    </>
  );

  const elNotAuthenticated = (
    <>
      <div className="card-body">
        
        {/* Konichiwa!!! */}

        <h2>You are not signed in yet.</h2>
        <p>By all means please login!</p>
        
        <NavLink role="button" to={""} className="btn btn-outline-success navbar-btn my-2 my-sm-0 mr-sm-2" onClick={login}>
          Login
        </NavLink>

      </div>
    </>
  );

  const elAuthenticated = (
    <>
      <div className="card-body">
        
        {/* Konichiwa!!! */}

        <h2>You are authenticated, baby!!!</h2>

        <NavLink role="button" to={""} className="btn btn-outline-danger navbar-btn my-2 my-sm-0 mr-sm-2" onClick={logout}>
          Logout
        </NavLink>

      </div>
    </>
  );

  const el = (
    <>
      <div className="py-5">
        <div className="row">
          <div className="col-md-6 mx-auto">
            
            <h1>HOME!</h1>
            
            <div className="card mt-4">
              
              { !authState.isAuthenticated && elNotAuthenticated }
              { authState.isAuthenticated && elAuthenticated }
              { authState.isPending && elPending }
              
            </div>

          </div>
        </div>
      </div>
    </>
  );

  return el;
}; 

export { Home };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/add-signin-button/ ]

Function-based components can use the useOktaAuth React hook to access the authService or the authState objects.

The okta-react SDK provides an authState object that provides information on the state of the current user's authentication. You can use the authState.isAuthenticated property, for example, to show or hide a button depending on whether the user is signed in.

The okta-react SDK provides an authService object that provides methods to read more details about, or to modify, the current authentication. The authService.login() method lets you specify the path you'd like the user to be navigated to after authenticating.

*/