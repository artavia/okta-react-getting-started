// FUNCTION BASED COMPONENT

import React from "react";

import { useState, useEffect, useCallback } from "react";

import { useOktaAuth } from "@okta/okta-react";

const Admin = () => {

  const { authState , authService } = useOktaAuth();
  const [ userInfo, setUserInfo ] = useState(null);

  const checkUser = useCallback( () => {
    
    // console.log( ">>>>> authState.isAuthenticated \n\n " , authState.isAuthenticated );
    // console.log( ">>>>> userInfo \n\n " , userInfo );
    
    // if user IS NOT authenticated, set userInfo to null
    if( !authState.isAuthenticated ){
      setUserInfo( null );
    }
    // if user IS authenticated, set userInfo to the response
    else {
      authService.getUser()
      .then( async (info) => {
        // console.log( ">>>>> await info \n\n " , await info );
        await setUserInfo( info );
      } );
    }
  } , [ authState, authService ] ); // Update if authState changes
  
  useEffect( () => { 

    checkUser();
    return () => {};

  } , [ checkUser ] ); // Update if checkUser changes
  
  const el = (
    <>
      <div className="py-5">
        <div className="row">
          <div className="col-md-6 mx-auto">
            
            <h1>Admin!</h1>
            
            <div className="card mt-4">

              <div className="card-body">

                {/* Konichiwa!!! */}
                
                { authState.isPending && ( <p>Your request is pending and loading&hellip;</p> ) }

                { userInfo !== null && (<p>Welcome back, { userInfo.name }!</p>) }

              </div>
              
            </div>

          </div>
        </div>
      </div>
    </>
  );

  return el;
};

export { Admin };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/user-info/ ]
SEE [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react ]

For function-based components, authState and authService are returned by the useOktaAuth hook. [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#useoktaauth ]

Your code can get the user's profile using the getUser() method [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservicegetuser ] on the AuthService object [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservice ]. You should wait until the authState.isAuthenticated flag is true.

*/