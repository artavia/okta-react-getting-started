// CLASS BASED COMPONENT

import React from "react";
import { withOktaAuth } from "@okta/okta-react";


class ClassyAdmin extends React.Component{
  
  constructor(props){
    super(props);
    this.state = { userInfo: null };

    this.checkUser = this.checkUser.bind(this);
  }

  async componentDidMount(){ 
    await this.checkUser();    
  }

  async componentDidUpdate(){
    await this.checkUser();
  }

  async checkUser(){
    if( this.props.authState.isAuthenticated && !this.state.userInfo ){
      const userInfo = await this.props.authService.getUser();
      this.setState( { userInfo: await userInfo } );
    }
  }
  

  render(){
    
    const el = (
      <>
        <div className="py-5">
          <div className="row">
            <div className="col-md-6 mx-auto">
              
              <h1>Admin!</h1>
              
              <div className="card mt-4">

                <div className="card-body">
                  
                  {/* Konichiwa!!! */}

                  { this.props.authState.isPending && (<p>Your request is pending and loading&hellip;</p>) }
                  
                  { this.state.userInfo !== null && (<p>Welcome back, { this.state.userInfo.name }!</p>) }

                </div>
                
              </div>

            </div>
          </div>
          
        </div>
      </>
    );
  
    return el;

  }

}

// export {ClassyAdmin};
const Admin = withOktaAuth( ClassyAdmin );
export { Admin };

/*
FROM [ https://developer.okta.com/docs/guides/sign-into-spa/react/user-info/ ]
SEE [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react ]

For class-based components, authState and authService are passed as props to your component via the withOktaAuth higher-order component. [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#withoktaauth ]

Your code can get the user's profile using the getUser() method [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservicegetuser ] on the AuthService object [ https://github.com/okta/okta-oidc-js/tree/master/packages/okta-react#authservice ]. You should wait until the authState.isAuthenticated flag is true.

*/