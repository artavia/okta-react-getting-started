import React from "react";
import { NavLink } from "react-router-dom";

const DNEPage = ( {location} ) => {
  
  const noOpLink = ( event ) => { 
    event.preventDefault(); 
  };
  
  const el = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">404, babe!</h1>
          <h2>No match found for <code>{location.pathname}</code></h2>
          <p>
            <NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to={""}>
              Learn more &raquo;
            </NavLink>
          </p>
        </div>
      </div>
      <div className="container">
        <p>Page Not Found.</p>
      </div>
    </>
  );

  return el;
};

export { DNEPage };
