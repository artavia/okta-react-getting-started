import React from "react";

import { Nav } from "./nav";

import { Footer } from "./footer";

const Main = ( props ) => {
  
  const el = (
    <div className="container">
      <Nav />
      <main role="main">
        { props.children }
      </main>
      <Footer />
    </div>
  );

  return el;
}

export { Main };
