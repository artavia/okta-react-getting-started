import React from "react";

import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useHistory } from 'react-router-dom';

import { Security, LoginCallback , SecureRoute } from "@okta/okta-react";

import { securityConfigurationObject } from "../custom-okta-security-configuration/config";
import { REACT_APP_REDIRECT_PATH } from "../custom-okta-security-configuration/development-environment-variables";

// import { Home } from "../components/home-function-based";
// import { Profile } from "../components/profile-function-based";
// import { Admin } from "../components/admin-function-based";

import { Home } from "../components/home-class-based";
import { Profile } from "../components/profile-class-based";
import { Admin } from "../components/admin-class-based";

import { Unauthenticated } from "../components/unauthenticated";
import { AlphaTest } from "../components/alphatest";
import { DNEPage } from "../DNE/DNEPage";
import { Main } from "./main";

const HasAccessToBrowserRouter = () => {
  
  const history = useHistory();

  const customizedSignInWidget = () => {
    history.push("/unauthenticated");
  };

  const element = (
    <>
      <Security {...securityConfigurationObject} onAuthRequired={ customizedSignInWidget }>
        <Main>
          <Switch>


            {/* FROM THE TOP DOWN THE ORDER MUST STAY THE SAME... */}

            <Route exact={true} path={ REACT_APP_REDIRECT_PATH } component={ LoginCallback } />

            <Route exact={true} path="/home" component={ Home }/>
            <SecureRoute exact={true} path="/admin" component={ Admin }/>
            <Route exact={true} path="/unauthenticated" component={ Unauthenticated }/>

            <SecureRoute exact={true} path="/profile" component={ Profile }/>
            <SecureRoute exact={true} path="/alphatest" component={ AlphaTest }/>
            
            {/* <Route path="/404" component={ DNEPage } /> */} {/* v1 - change the url  */}
            {/* <Redirect exact={true} from="/" to="/home" /> */} {/* v1 - change the url  */}
            {/* <Redirect from="*" to="/404" /> */} {/* v1 - change the url  */}
            
            <Redirect exact={true} from="/" to="/home" /> {/* v2 - DO NOT change the url */}
            <Route component={ DNEPage } /> {/* v2 - DO NOT change the url */}

          </Switch>
        </Main>
      </Security>
    </>
  );

  return element;
};

// NEW EXPORT
const Alpha = () => {

  // In order for the Nav to behave properly, the order of the stack is changed 
  // from ... BrowserRouter > Main > Security > Switch > ETC...
  // to   ... BrowserRouter > Security > Main > Switch > ETC...
  
  const el = (
    <BrowserRouter>
      <HasAccessToBrowserRouter />
    </BrowserRouter>
  );

  return el;
}; 

export { Alpha };
