# Okta React: Getting Started

## Description
Taking **okta-react** for a test run with ReactJS.

### Processing and/or completion date(s)
July 11, 2020

## A brief comment
In my endeavour to practice with a dev stack it became obvious that some of the moving pieces had to be switched around.

This is not the final version that is found in another later project but it does provide a good push in the right direction. 


## God bless!
God has a plan and an agenda for me. I hope you can say the same.

Help me if you are able. I thank you.